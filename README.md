**Jira Groovy Tools**

GUI/CLI application for making REST API operations in JIRA. Simplify routine tasks for administrating and coolect 
statistic information from JIRA.

**Functions**
* Sync components between projects - compare/copy/sync components between different projects
* Check rapid boards - statistics for rapid views
* Status statistics - statistics for statuses
* Update custom fields from CSV file

**Download**

[Version 1.0.1](https://andrewsite.ru/wp-content/uploads/2020/07/jgt.jar)
[Version 1.0.2](https://andrewsite.ru/wp-content/uploads/2020/07/jira-groovy-tools-1.0.2-all.jar)

**Author**
[AndrewDvizhok@gmail.com](mailto:andrewdvizhok@gmail.com)

Web site
[www.andrewsite.ru](https://andrewsite.ru)


---

## Sync components between projects

Compare list of two projects, copy (copy with replace, full sync) components from one projects to another. 

### GUI

1. Run jar app use command
**java -jar jgt.jar**
2. App show dialog window for asking parameters. Provide URL for jira server (https://jira.example.com),
 username and password for login. If Your server hasn't trusted cert set checkBox
  *'Disable check certificate'*
3. Click *Login* button. Application check authentication by trying get profile info
4. In next window click button **Sync components**
5. App ask **project key** for projects. In the left field type key of the project **from** components
 should be copied, in the right field provide key of the project **to** components will be copied. Both fields are case
  insensitive
6. **Compare project** - generate CSV file with table of comparing both components list. This task executed 
always before other tasks
7. **Copy w/o replace** - copy components, but if component name in **to** the same as in **from** app will save 
values as in **to**. Example: components of project **from** [A, B, C], component of project **to** [C, D, E]. After 
copy the project **to** will be store components [A, B, C, D, E] (component C will be the same as in **to** project)
8. **Copy with replace** - copy components, but if component name in **to** the same as in **from** app will save
 values as in **from**. Example: components of project **from** [A, B, C], component of project **to** [C, D, E]. 
 After copy the project **to** will be store components [A, B, C, D, E] (component C will be the same as in **from**
  project)
9. **Sync components** - copy with replace and remove from **to** project all components which no exist in **from**.
Example: components of project **from** [A, B, C], component of project **to** [C, D, E]. After copy the project 
**to** will be store components [A, B, C] (component C will be the same as in **from** project)

### CLI
1. Run jar app use command
**java -DjiraUrl=https://jira.example.com -DjiraUser=name -DjiraPass=password -DjiraTool=compSync -DfromProj=KEYA 
-DtoProj=KEYB -DtaskComp=compare -jar jgt.jar** Task should be: compare, copy, copyr, sync

Application create CSV file with name *projKeyFrom-projKeyTo-timestamp.csv*

---

## Check rapid view

Generate CSV file with statistics of response time, count issues, administrators, filters for rapid boards.

### GUI

1. ...
2. ...
3. ...the same as in previous
4. Click **Run** in section **Check rapid view**
5. If Jira has a lots board app will show progress bar
6. After app done analysing it will popup message about CSV files

### CLI

1. Run jar app use command
**java -DjiraUrl=https://jira.example.com -DjiraUser=name -DjiraPass=password -jar jgt.jar**

## Get statuses statistics

Generate CSV file with statistics of the jira statuses. Stats data consist of "used in project" 
(how many project has workflows with that status), "used in issue type" (how many issue types associated
with workflow who used this status), "used in issues" (how many issues are in this status).

###GUI
1. ...
2. ...
4. Click **Run** in section **Get status stats**
5. If Jira has a lots board app will show progress bar
6. After app done analysing it will popup message about CSV files

### CLI

1. Run jar app use command
 **java -DjiraUrl=https://jira.example.com -DjiraUser=name -DjiraPass=password 
 -jar -DjiraTool=status jgt.jar**

## Update custom fields from CSV
If you forgot to import a field from a CSV backup, you can update the fields
 in an existing project. Format CSV file the same as in exported CSV from jira.
 Mandatory fields 'Issue key', custom fields should look like 'Custom Field 
 (Epic Link)'
 
 ###CLI
 
  **java -DjiraUrl=https://jira.example.com -DjiraUser=name -DjiraPass=password 
  -jar -DjiraTool=csvfield -Dcsv=/Path/to/csv/file.csv jgt.jar**
 