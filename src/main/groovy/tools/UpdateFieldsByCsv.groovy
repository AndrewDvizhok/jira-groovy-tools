package tools

import base.CsvTable
import base.JiraConnector
import base.UiRunner

import javax.swing.JFrame

class UpdateFieldsByCsv implements JiraConnector, UiRunner {
    @Override
    void runUi(JFrame parentFrame) {

    }

    boolean run(String csvFilePath){
        CsvTable table = new CsvTable(new File(csvFilePath.toString()))
        if(table.data != null && table.data.getAt("Issue key") != null){
            table.data["Issue key"].each { rowN, issueKey->
                def fields = table.getFieldsByIssue(issueKey)
                if(fields != null && fields.size()>0 ){
                    updateCustomFields(issueKey, fields)
                }

            }
        }
    }
}
