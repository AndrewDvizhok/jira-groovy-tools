package tools

import base.CsvSaver
import base.JiraConnector
import base.UiRunner
import groovy.json.JsonSlurper
import javafx.scene.control.ProgressBar
import javafx.util.Pair
import jira.JiraProject
import jira.JiraRapidView
import jira.JiraStatus

import javax.swing.JFrame
import javax.swing.JOptionPane
import javax.swing.ProgressMonitor

class StatusStatistic implements JiraConnector, UiRunner, CsvSaver {

    @Override
    void runUi(JFrame parentFrame) {
        new Thread({
            Set statuses = getStatuses()
            def progressMax = statuses.size() + getProjects().size()
            ProgressMonitor progressMonitor = new ProgressMonitor(parentFrame, "Check statuses",
                    "Checking...", 0, progressMax)
            run(progressMonitor)
            JOptionPane.showMessageDialog(parentFrame, "CSV file StatusStat was saved!")

        }).start()
    }

    void run(ProgressMonitor progressMonitor = null) {
        Set statusSet = getStatuses()
        measureStatusSet(statusSet, progressMonitor)
        def header = "id,name,description,color,usedInProjects,usedInIssueTypes,usedInIssues"
        saveCsvFile(statusSet, "StatusStat", header)
    }

    void measureStatusSet(Set statusSet, ProgressMonitor progressMonitor = null) {
        Map<JiraStatus, Set> usedInProject = statusSet.collectEntries { status -> [status, new HashSet<>()] }
        Map<JiraStatus, Set> usedInIssueType = statusSet.collectEntries { status -> [status, new HashSet<>()] }
        Map<JiraStatus, Pair<Set, Set>> usabilityStatusProjectType = statusSet.collectEntries { status ->
            [status, new Pair(new HashSet<>(), new HashSet<>())]
        }
        int progress = 0
        getProjects().each { JiraProject project ->
            try {
                new JsonSlurper().parseText(
                        getRequest("/rest/api/latest/project/${project.projectKey}/statuses")
                ).each { issueType ->
                    if (issueType == null) {
                        throw new NullPointerException('API return null project!')
                    }
                    issueType.statuses.each { status ->
                        JiraStatus jiraStatus = statusSet.find { it.id == status.id.toInteger() }
                        if (jiraStatus != null) {
                            usabilityStatusProjectType[jiraStatus].key.add(project.projectId)
                            usabilityStatusProjectType[jiraStatus].value.add(issueType.id.toInteger())
                        }
                    }
                }
            } catch (Exception e) {
                println "error on ${project} statuses"
                println e.getMessage()
                e.getStackTrace().each { println it }
            }

            if (progressMonitor != null) {
                progress++
                progressMonitor.setProgress(progress)
            }

        }

        usabilityStatusProjectType.each { status, pair ->
            status.usedInProjects = pair.key.size()
            status.usedInIssueTypes = pair.value.size()
            status.usedInIssues = getCountIssuesByJql("status='${URLEncoder.encode(status.name, "UTF-8")}'")
            if (progressMonitor != null) {
                progress++
                progressMonitor.setProgress(progress)
            }
        }

    }


}
