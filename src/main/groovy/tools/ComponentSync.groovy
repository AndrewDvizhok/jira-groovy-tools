package tools

import base.JiraConnector
import base.MigrateComponents
import base.UiRunner
import jira.JiraComponent

import javax.swing.BorderFactory
import javax.swing.JButton
import javax.swing.JFrame
import javax.swing.JLabel
import javax.swing.JOptionPane
import javax.swing.JPanel
import javax.swing.JTextField
import java.awt.GridLayout
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.nio.file.FileAlreadyExistsException

class ComponentSync implements JiraConnector, MigrateComponents, UiRunner {

    @Override
    def compareComponents(Object leftProjectKey, Object rightProjectKey) {
        def date = new Date()
        Set fromComps = getComponents(leftProjectKey)
        Set toComps = getComponents(rightProjectKey)
        try {
            File compareCompFile = new File("${leftProjectKey}-${rightProjectKey}-${date.getTime()}.csv")
            compareCompFile.write("Name-${leftProjectKey},Desc,Lead,Assignee,Name-${rightProjectKey},Desc,Lead,Assignee\n")
            fromComps.each { fromComponent ->
                if (toComps.find { toComponent -> toComponent.name == fromComponent.name }) {
                    def toComponent = toComps.find { toComponent -> toComponent.name == fromComponent.name }
                    compareCompFile.append("${fromComponent},${toComponent}\n")
                } else {
                    compareCompFile.append("${fromComponent}\n")
                }
            }
            toComps.each { toComponent ->
                if (!fromComps.find { fromComponent -> fromComponent.name == toComponent.name }) {
                    compareCompFile.append(",,,,${toComponent}\n")
                }
            }
        } catch (FileAlreadyExistsException e) {
            println "File already exist, can't recreate ${e.getMessage()}"
        } finally {
            return [from: fromComps, to: toComps]
        }
    }

    @Override
    def unionComponentsNoReplace(Object fromProject, Object toProject) {
        def components = compareComponents(fromProject, toProject)
        components.from.each { JiraComponent fromComponent ->
            JiraComponent toComponent = components.to.find { JiraComponent toComponent -> toComponent.name == fromComponent.name }
            if (toComponent == null) {
                createComponent(fromComponent)
            }
        }
        components
    }

    @Override
    def unionComponentsWithReplace(Object fromProject, Object toProject) {
        def components = unionComponentsNoReplace(fromProject, toProject)
        components.from.each { JiraComponent fromComponent ->
            JiraComponent toComponent = components.to.find { toCopmonent -> toCopmonent.name == fromComponent.name }
            if (toComponent != null) {
                updateComponent(fromComponent, toComponent)
            }
        }
        components
    }

    @Override
    def syncComponents(Object fromProject, Object toProject) {
        def components = unionComponentsWithReplace(fromProject, toProject)
        components.to.each { JiraComponent toComponent ->
            if (!components.from.find { fromCopmonent -> fromCopmonent.name == toComponent.name }) {
                deleteComponent(toComponent)
            }
        }
        components
    }

    @Override
    void runUi(JFrame parent) {

        def jfProjects = new JFrame("Set projects")
        jfProjects.setLocationRelativeTo(null)
        jfProjects.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE)
        def lblFromProject = new JLabel("FROM")
        def fromProject = new JTextField()
        lblFromProject.setLabelFor(fromProject)

        def lblToProject = new JLabel("TO")
        def toProject = new JTextField()
        lblToProject.setLabelFor(toProject)

        def jbCompareComponents = new JButton("Compare component")
        def jbUnionComponents = new JButton("Copy w/o replace")
        def jbUnionComponentsReplace = new JButton("Copy with replace")
        def jbSyncComponents = new JButton("Sync components")

        jbCompareComponents.addActionListener(new ActionListener() {
            @Override
            void actionPerformed(ActionEvent e) {
                if (!fromProject.getText().empty && !toProject.getText().empty) {
                    compareComponents(fromProject.getText().toUpperCase(), toProject.getText().toUpperCase())
                    JOptionPane.showMessageDialog(jfProjects, "CSV file was saved!")
                }
            }
        })

        jbUnionComponents.addActionListener(new ActionListener() {
            @Override
            void actionPerformed(ActionEvent e) {
                if (isFieldsNotEmpty(fromProject, toProject)) {
                    unionComponentsNoReplace(fromProject.getText().toUpperCase(), toProject.getText().toUpperCase())
                }
            }
        })

        jbUnionComponentsReplace.addActionListener(new ActionListener() {
            @Override
            void actionPerformed(ActionEvent e) {
                if (isFieldsNotEmpty(fromProject, toProject)) {
                    unionComponentsWithReplace(fromProject.getText().toUpperCase(), toProject.getText().toUpperCase())
                }
            }
        })

        jbSyncComponents.addActionListener(new ActionListener() {
            @Override
            void actionPerformed(ActionEvent e) {
                if (isFieldsNotEmpty(fromProject, toProject)) {
                    syncComponents(fromProject.getText().toUpperCase(), toProject.getText().toUpperCase())
                }
            }
        })

        def panel = new JPanel()
        panel.setLayout(new GridLayout(4, 2, 10, 10))
        panel.setBorder(BorderFactory.createTitledBorder("Sync components"))
        panel.add(lblFromProject)
        panel.add(lblToProject)
        panel.add(fromProject)
        panel.add(toProject)
        panel.add(jbCompareComponents)
        panel.add(jbUnionComponents)
        panel.add(jbUnionComponentsReplace)
        panel.add(jbSyncComponents)

        jfProjects.setSize(400, 247)
        jfProjects.getContentPane().add(panel)
        jfProjects.setVisible(true)
    }

    boolean run(fromProj, toProj, task) {
        def result = true
        switch (task) {
            case "compare":
                compareComponents(fromProj, toProj)
                break
            case "copy":
                unionComponentsNoReplace(fromProj, toProj)
                break
            case "copyr":
                unionComponentsWithReplace(fromProj, toProj)
                break
            case "sync":
                syncComponents(fromProj, toProj)
                break
            default: result = false
        }
        result
    }

}