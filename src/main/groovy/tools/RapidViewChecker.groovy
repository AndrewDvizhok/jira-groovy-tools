package tools

import base.CsvSaver
import base.JiraConnector
import base.UiRunner
import groovy.json.JsonSlurper
import jira.JiraRapidView

import javax.swing.JFrame
import javax.swing.JOptionPane
import javax.swing.JPanel
import javax.swing.JProgressBar
import javax.swing.ProgressMonitor
import java.nio.file.FileAlreadyExistsException
import java.time.Duration
import java.time.Instant
import java.util.concurrent.TimeUnit

class RapidViewChecker implements JiraConnector, UiRunner, CsvSaver {

    void measureRapidView(JiraRapidView rapidView) {
        try {
            Instant start = Instant.now()
            def responseText = getRequest("/rest/greenhopper/1.0/xboard/work/allData.json?rapidViewId=${rapidView.id}")
            if (lastResponseStatus.toString().startsWith("2")) {
                def json = new JsonSlurper().parseText(
                        responseText
                )
                Instant finish = Instant.now()
                rapidView.responseTimeJson = Duration.between(start, finish).toMillis()
                rapidView.countIssues = json.issuesData.issues.size()

                start = Instant.now()
                getRequest("/secure/RapidBoard.jspa?rapidView=${rapidView.id}")
                finish = Instant.now()
                rapidView.responseTimeHtml = Duration.between(start, finish).toMillis()
            } else {
                rapidView.responseTimeJson = "Error: response code ${lastResponseStatus}"
            }


        } catch (Exception e) {
            println "error on get rapidView ${id}"
            println e.getMessage()
            e.getStackTrace().each { println it }
        }
    }

    @Override
    void runUi(JFrame parent) {
        new Thread({
            def rapidViews = getRapidViews()
            ProgressMonitor progressMonitor = new ProgressMonitor(parent, "Check rapid views",
                    "Checking...", 0, rapidViews.size())
            int progress = 0
            def csvFileName = "CheckRapidViews-${new Date().getTime()}.csv"
            rapidViews.each { JiraRapidView rapidView ->
                measureRapidView(rapidView)
                progress++
                progressMonitor.setProgress(progress)
            }
            saveCsvFile(rapidViews)
            JOptionPane.showMessageDialog(parent, "CSV file ${csvFileName} was saved!")
        }).start()
    }

    void run() {
        Set rapidViews = getRapidViews()
        rapidViews.each { rapidView->
            measureRapidView(rapidView)
        }
        def header = "id, name, has sprint, filter id, filter query, filter owner, board admins, count issues, response time json(ms), response time html(ms)"
        saveCsvFile(rapidViews, "CheckRapidView", header)
    }

}