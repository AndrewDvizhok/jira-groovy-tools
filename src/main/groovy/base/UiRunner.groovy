package base

import javax.swing.JFrame

interface UiRunner {
    void runUi(JFrame parentFrame);
}