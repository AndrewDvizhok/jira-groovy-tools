package base

final class ParametersStorage {
    static String targetHost
    static String credentials
    static String account

    static void setCredentials(name, pass) {
        account = name
        credentials = "${name}:${pass}".getBytes().encodeBase64().toString()
    }
}