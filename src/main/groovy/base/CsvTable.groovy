package base

class CsvTable {
    Map<Integer, String> header
    Map<String, Map<Integer,String>> data // Map<header, Map<#row, value>>

    CsvTable(File csvFile){
        if(csvFile.exists()){
            data = new HashMap<>()
            header = new HashMap<>()
            boolean firstLine = true
            int rowNumber = 0
            csvFile.readLines().each {row->
                if(firstLine){
                    int colNum = 0
                    parseCsvRow(row).each { column->
                        header.put(colNum, column)
                        data.put(column,new HashMap<Integer, String>())
                        colNum++
                    }
                    firstLine = false
                }else{
                    List parsedRow = parseCsvRow(row)
                    for(int i=0; i<parsedRow.size(); i++){
                        def column = header.getAt(i)
                        def valueOnColumn = parsedRow.get(i)
                        data.get(column).put(rowNumber,valueOnColumn)
                    }
                    rowNumber++
                }
            }
        }else{
            println "File not found: ${csvFile}"
        }
    }

    Map<String,String> getPairHeaders(String headerKey, String headerValue){
        Map paired = new HashMap()
        Map key = data.getAt(headerKey)
        Map value = data.getAt(headerValue)
        for(int i=0; i<key.size(); i++){
            paired.put(key.get(i),value.get(i))
        }
        paired
    }

    Map<String,String> getFieldsByIssue(String issue){
        Map<String, String> fields = new HashMap<>()
        def row = data.getAt("Issue key").find{ num, issueKey-> issueKey == issue }
        if(row != null){
            int rowNumber = row.key
            data.each { field, map->
                if(field != "Issue key"){
                    def valInCell = map.getAt(rowNumber)
                    if(valInCell != null){
                        fields.put(field, valInCell)
                    }

                }
            }
        }else{
            println "No exist issue in csv: ${issue}"
        }
        fields
    }

    boolean has(String name){
        data[name] != null && data[name].size() != 0
    }

    List<String> parseCsvRow(String row){
        List<String> parsedRow = []
        boolean inQuotes = false
        String word = new String()
        row.each{ch->

            if (inQuotes) {
                if(ch == '"'){
                    inQuotes = false
                }else{
                    word += ch
                }
            } else {
                if(ch == '"'){
                    inQuotes = true
                }else{
                    if (ch == ',') {
                        parsedRow.add(word)
                        word = new String()
                    }else{
                        word += ch
                    }
                }
            }
        }
        if(word != ""){
            parsedRow.add(word)
        }
        parsedRow
    }
}
