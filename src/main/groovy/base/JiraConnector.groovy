package base

import base.HttpRequestAbility
import groovy.json.JsonSlurper
import jira.JiraComponent
import jira.JiraProject
import jira.JiraRapidView
import jira.JiraStatus
import jira.Jiras

import java.lang.invoke.MethodHandleImpl

trait JiraConnector implements HttpRequestAbility {

    Set<JiraComponent> getComponents(String projectKey) {
        def components = [] as Set
        try {
            new JsonSlurper().parseText(
                    getRequest("/rest/api/latest/project/${projectKey}/components")
            ).each {
                if (it == null) {
                    throw new NullPointerException('API return null project!')
                }
                JiraComponent jiraComponent = new JiraComponent(it.id, it.name.replaceAll("\"", "\"\""), it.assigneeType)
                if (it.description != null) {
                    jiraComponent.description = it.description.replaceAll("\"", "\"\"")
                }
                if (it.lead != null) {
                    jiraComponent.lead = it.lead.name
                }
                components.add(jiraComponent)
            }
        } catch (Exception e) {
            println "error on ${projectKey}"
            println e.getMessage()
            e.getStackTrace().each { println it }
        }
        return components
    }

    String createComponent(JiraComponent jComp) {

        def json = """
        {"name": "${jComp.name}",
        "assigneeType": "${jComp.assigneeType}",
        "project": "${jComp.project}"
        """
        if (jComp.description != null) {
            json += ",\"description\": \"${jComp.description}\""
        }
        if (jComp.lead != null) {
            json += ",\"leadUserName\": \"${jComp.lead}\""
        }
        json += "}"

        postRequest("/rest/api/latest/component", json)

    }

    String updateComponent(JiraComponent from, JiraComponent to) {
        def descr = ""
        def lead = ""
        if (from.description != null) {
            descr = from.description
        }
        if (from.lead != null) {
            lead = from.lead
        }
        def json = """{
            "assigneeType": "${from.assigneeType}",
            "description": "${descr}",
            "leadUserName": "${lead}"
            }"""

        postRequest("/rest/api/latest/component/${to.id}", json, "PUT")
    }

    String deleteComponent(JiraComponent component) {
        getRequest("/rest/api/2/component/${component.id}", "DELETE")
    }

    Set<JiraRapidView> getRapidViews() {
        Set<JiraRapidView> rapidViews = new HashSet<>()
        try {
            new JsonSlurper().parseText(
                    getRequest("/rest/greenhopper/1.0/rapidviews/viewsData")
            ).views.each {
                JiraRapidView jiraRapidView = new JiraRapidView()
                jiraRapidView.id = it.id
                jiraRapidView.name = it.name.replaceAll("\"", "\"\"")
                jiraRapidView.sprint = it.sprintSupportEnabled
                jiraRapidView.filter = it.filter.id
                jiraRapidView.filterOwner = it.filter.owner.userName
                jiraRapidView.query = it.filter.query.replaceAll("\"", "\"\"")
                it.boardAdmins.userKeys.each { userKey ->
                    jiraRapidView.addBoardAdmin(userKey.key)
                }
                it.boardAdmins.groupKeys.each { groupKey ->
                    jiraRapidView.addBoardAdmin(groupKey.key)
                }

                rapidViews.add(jiraRapidView)
            }
        } catch (Exception e) {
            println "error on get rapidViews"
            println e.getMessage()
            e.getStackTrace().each { println it }
        }
        rapidViews
    }

    Set<JiraStatus> getStatuses() {
        Set<JiraStatus> statusSet = new HashSet<>()
        try {
            new JsonSlurper().parseText(
                    getRequest("/rest/api/latest/status")
            ).each {
                if (it == null) {
                    throw new NullPointerException('API return null project!')
                }
                JiraStatus jiraStatus = new JiraStatus(it.id, it.name, it.description, it.statusCategory.colorName)
                statusSet.add(jiraStatus)
            }
        } catch (Exception e) {
            println "error on /rest/api/latest/status"
            println e.getMessage()
            e.getStackTrace().each { println it }
        }
        return statusSet
    }

    Set<JiraProject> getProjects() {
        Set<JiraProject> projects = new HashSet<>()
        try {
            new JsonSlurper().parseText(
                    getRequest("/rest/api/latest/project")
            ).each {
                if (it == null) {
                    throw new NullPointerException('API return null project!')
                }
                JiraProject project = new JiraProject(it.id, it.key, it.name)
                projects.add(project)
            }
        } catch (Exception e) {
            println "error on /rest/api/latest/project"
            println e.getMessage()
            e.getStackTrace().each { println it }
        }
        projects
    }

    int getCountIssuesByJql(String jql) {
        int result = -1
        try {
            def response = new JsonSlurper().parseText(getRequest("/rest/api/latest/search?jql=${jql}"))

            if (response == null) {
                throw new NullPointerException('API return null jql!')
            }
            result = response.total.toInteger()
        }
        catch (
                Exception e
                ) {
            println "error on search?jql=${jql}"
            println e.getMessage()
            e.getStackTrace().each { println it }
        }
        result
    }

    String getFieldIdByName(String name) {
        String fieldId
        try {
            Set fields = new JsonSlurper().parseText(
                    getRequest("/rest/api/latest/customFields?search="+URLEncoder.encode(name, "UTF-8"))
            ).values

            if (fields.size() == 0) {
                throw new NullPointerException('Not found fields')
            } else {
                fieldId = fields.getAt(0).id
            }
        } catch (Exception e) {
            println "error on /rest/api/2/customFields?search=" + name
            println e.getMessage()
            e.getStackTrace().each { println it }
        }
        fieldId
    }

    String updateCustomField(String ticket, String fieldId, String value) {
        def json = """{
            "fields": {
                "${fieldId}": "${value}"
            }
        }"""

        postRequest("/rest/api/2/issue/${ticket}", json, "PUT")
    }

    String updateCustomFields(String ticket, Map<String, String> fields) {
        String fieldsString = ""
        int fieldNumber = 1
        def sizeFields = fields.size()
        fields.each { field, value ->
            def fieldReg = (field =~ "Custom field\\s\\((.*)\\)")
            if (fieldReg.size() > 0) {
                if (fieldReg[0].size() > 1){
                    def fieldId = getFieldIdByName(fieldReg[0][1])
                    fieldsString += "\"${fieldId}\":\"${value}\""
                    if (fieldNumber < fields.size()) {
                        println "size fields: ${fields.size()}"
                        fieldsString += ","
                        fieldNumber++
                    }
                }
            }
        }

        def json = """{
            "fields": {
                ${fieldsString}
            }
        }"""

        println json

        postRequest("/rest/api/2/issue/${ticket}", json, "PUT")
    }

}