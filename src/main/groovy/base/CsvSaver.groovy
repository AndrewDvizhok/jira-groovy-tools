package base

import jira.Jiras

import java.nio.file.FileAlreadyExistsException

trait CsvSaver {

    void saveCsvFile(Set items, String fileName, String csvHeader) {
        try {
            def csvFileName = "${fileName}-${new Date().getTime()}.csv"
            File csvFile = new File(csvFileName)
            csvFile.write("${csvHeader}\n")
            items.each { item ->
                csvFile.append("${item}\n")
            }
        } catch (FileAlreadyExistsException e) {
            println "File already exist, can't recreate ${e.getMessage()}"
        }
    }

}