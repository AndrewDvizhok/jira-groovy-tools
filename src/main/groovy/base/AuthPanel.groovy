package base

import tools.ComponentSync
import tools.RapidViewChecker
import tools.StatusStatistic

import javax.net.ssl.*
import javax.swing.*
import java.awt.*
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.awt.datatransfer.StringSelection
import java.awt.datatransfer.Clipboard

class AuthPanel implements HttpRequestAbility {

    private JFrame frame
    private static ComponentSync compSync
    private static RapidViewChecker checkerBoards
    private static StatusStatistic statusStat

    void askLoginInfo() {

        def frame = new JFrame("Parameters")

        frame.setLocationRelativeTo(null)

        def lblUrl = new JLabel("Jira URL:", SwingConstants.RIGHT)
        def jiraUrl = new JTextField(20)
        lblUrl.setLabelFor(jiraUrl)

        def lblUser = new JLabel("User Name:", SwingConstants.RIGHT)
        def tfUser = new JTextField(20)
        lblUser.setLabelFor(tfUser)

        def lblPassword = new JLabel("Password:", SwingConstants.RIGHT)
        def final pfPassword = new JPasswordField(20)
        lblPassword.setLabelFor(pfPassword)

        JButton btnGet = new JButton("Display Password")
        btnGet.addActionListener(
                new ActionListener() {

                    void actionPerformed(ActionEvent e) {
                        def password = new String(pfPassword.getPassword())
                        JOptionPane.showMessageDialog(frame,
                                "Password is " + password)
                    }
                })

        def btnLogin = new JButton("Login")

        def lblCert = new JLabel("Disable check certificate")
        def cbCert = new JCheckBox()
        lblCert.setLabelFor(cbCert)

        def lblSrc = new JLabel("Author AndrewDvizhok@gmail.com")
        def btnSrc = new JButton("Copy src address")
        lblSrc.setLabelFor(btnSrc)

        btnLogin.addActionListener(new ActionListener() {
            @Override
            void actionPerformed(ActionEvent e) {

                if (jiraUrl.getText().lastIndexOf("/") == jiraUrl.getText().length() - 1) {
                    ParametersStorage.targetHost = jiraUrl.getText().substring(0, jiraUrl.getText().length() - 1)
                } else {
                    ParametersStorage.targetHost = jiraUrl.getText()
                }

                ParametersStorage.setCredentials(tfUser.getText(), pfPassword.getPassword())
                if (cbCert.isSelected()) {
                    disableSslForConnection()
                }
                if (checkConnection()) {
                    frame.setVisible(false)
                    selectToolPanel()
                } else {
                    JOptionPane.showMessageDialog(
                            frame,
                            "Can't login! Please check login/name or try trust all cert",
                            "Authentication error",
                            JOptionPane.ERROR_MESSAGE)
                }

            }
        })

        btnSrc.addActionListener(new ActionListener() {
            @Override
            void actionPerformed(ActionEvent e) {
                String srcLink = "https://bitbucket.org/AndrewDvizhok/jira-groovy-tools/src/master/";
                StringSelection stringSelection = new StringSelection(srcLink);
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard() as Clipboard;
                clipboard.setContents(stringSelection, null);
            }
        })

        def panel = new JPanel()
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10))
        panel.setLayout(new GridLayout(6, 2, 10, 10))

        panel.add(lblUrl)
        panel.add(jiraUrl)
        panel.add(lblUser)
        panel.add(tfUser)
        panel.add(lblPassword)
        panel.add(pfPassword)
        panel.add(btnLogin)
        panel.add(btnGet)
        panel.add(lblCert)
        panel.add(cbCert)
        panel.add(lblSrc)
        panel.add(btnSrc)

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
        frame.setSize(400, 247)
        frame.getContentPane().add(panel)
        frame.setVisible(true)
    }

    void selectToolPanel() {
        frame = new JFrame("Tools")
        frame.setLocationRelativeTo(null)


        def lblSyncComp = new JLabel("Sync components", SwingConstants.CENTER)
        def syncComp = new JButton("Set projects")
        lblSyncComp.setLabelFor(syncComp)

        def lblCheckBoards = new JLabel("Check rapid views", SwingConstants.CENTER)
        def checkBoards = new JButton("Run")
        lblCheckBoards.setLabelFor(checkBoards)

        def lblStatusStat = new JLabel("Get statuses stats", SwingConstants.CENTER)
        def statusStatBtn = new JButton("Run")
        lblStatusStat.setLabelFor(statusStatBtn)

        syncComp.addActionListener(new ActionListener() {
            @Override
            void actionPerformed(ActionEvent e) {
                if (compSync == null) {
                    compSync = new ComponentSync()
                }
                compSync.runUi()
            }
        })

        checkBoards.addActionListener(new ActionListener() {
            @Override
            void actionPerformed(ActionEvent e) {
                if (checkerBoards == null) {
                    checkerBoards = new RapidViewChecker()
                }
                checkerBoards.runUi(frame)
            }
        })

        statusStatBtn.addActionListener(new ActionListener() {
            @Override
            void actionPerformed(ActionEvent e) {
                if (statusStat == null) {
                    statusStat = new StatusStatistic()
                }
                statusStat.runUi(frame)
            }
        })

        def panel = new JPanel()
        panel.setLayout(new GridLayout(3, 1))
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10))

        def syncToolPanel = new JPanel()
        def checkRapidPanel = new JPanel()
        def statusStatPanel = new JPanel()
        syncToolPanel.setBorder(BorderFactory.createTitledBorder("Sync components"))
        checkRapidPanel.setBorder(BorderFactory.createTitledBorder("Check rapid views"))
        statusStatPanel.setBorder(BorderFactory.createTitledBorder("Get statuses stats"))
        syncToolPanel.setLayout(new GridLayout(1, 2))
        checkRapidPanel.setLayout(new GridLayout(1, 2))
        statusStatPanel.setLayout(new GridLayout(1, 2))
        syncToolPanel.add(lblSyncComp)
        syncToolPanel.add(syncComp)

        checkRapidPanel.add(lblCheckBoards)
        checkRapidPanel.add(checkBoards)

        statusStatPanel.add(lblStatusStat)
        statusStatPanel.add(statusStatBtn)

        panel.add(syncToolPanel)
        panel.add(checkRapidPanel)
        panel.add(statusStatPanel)

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
        frame.setSize(400, 247)
        frame.getContentPane().add(panel)
        frame.setVisible(true)
    }

    boolean checkConnection() {
        getRequest("/rest/api/2/user?username=" + ParametersStorage.account)
        (lastResponseStatus == 200)
    }

    static void disableSslForConnection() {
        TrustManager[] trustAllCerts = [
                new X509TrustManager() {
                    @Override
                    void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                    }

                    @Override
                    void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                    }

                    X509Certificate[] getAcceptedIssuers() {
                        return null
                    }
                }
        ]

        def sc = SSLContext.getInstance("SSL")
        sc.init(null, trustAllCerts, new java.security.SecureRandom())

        def allHostsValid = new HostnameVerifier() {
            boolean verify(String hostname, SSLSession session) {
                return true
            }
        }
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory())
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid)
    }

}