package base

trait HttpRequestAbility {

    def lastResponseStatus

    String getRequest(address, method = "GET") {
        try {
            def connection = "${ParametersStorage.targetHost}${address}".toURL().openConnection()
            connection.addRequestProperty("Authorization", "Basic ${ParametersStorage.credentials}")
            connection.setRequestMethod(method)
            connection.doOutput = false
            connection.connect()
            lastResponseStatus = connection.responseCode
            def response = connection.content.text
            println "Call ${address} by ${method}, got ${connection.responseCode}"
            if (!lastResponseStatus.toString().startsWith("2")) {
                println response
            }
            return response
        } catch (Exception e) {
            println e.getMessage()
            e.getStackTrace().each { println it }
        }
    }

    String postRequest(address, body, method = "POST") {
        try {
            def connection = "${ParametersStorage.targetHost}${address}".toURL().openConnection()
            connection.addRequestProperty("Authorization", "Basic ${ParametersStorage.credentials}")
            connection.addRequestProperty("Content-Type", "application/json")
            connection.setRequestMethod(method)
            connection.doOutput = true
            connection.outputStream.withWriter {
                it.write(body)
                it.flush()
            }
            connection.connect()
            lastResponseStatus = connection.responseCode
            println "Call ${address} by ${method}, got ${connection.responseCode}"
            if (!lastResponseStatus.toString().startsWith("2")) {
                println body
            }
        } catch (Exception e) {
            println e.getMessage()
            e.getStackTrace().each { println it }
        }
    }

}