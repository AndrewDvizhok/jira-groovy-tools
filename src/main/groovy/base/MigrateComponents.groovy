package base

interface MigrateComponents {
    def compareComponents(leftProject, rightProject)
    def unionComponentsNoReplace(fromProject, toProject)
    def unionComponentsWithReplace(fromProject, toProject)
    def syncComponents(fromProject, toProject)
}