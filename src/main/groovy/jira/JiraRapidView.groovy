package jira

class JiraRapidView {
    def id
    String name
    String sprint
    String filter
    String query
    String filterOwner
    Set<String> boardAdmins
    int countIssues
    String responseTimeJson
    String responseTimeHtml

    void addBoardAdmin(name){
        if(boardAdmins == null){
            boardAdmins = new HashSet()
        }
        boardAdmins.add(name)
    }

    @Override
    String toString() {
        String admins = ""
        if(boardAdmins != null){
            boardAdmins.each {admin->
                admins += "${admin};"
            }
        }
        return "${id},\"${name}\",${sprint},${filter},\"${query}\",${filterOwner},${admins},${countIssues},${responseTimeJson},${responseTimeHtml}"
    }
}