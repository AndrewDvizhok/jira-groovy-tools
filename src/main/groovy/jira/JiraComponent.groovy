package jira

class JiraComponent {
    def id
    def name
    def description
    def lead
    def assigneeType
    def project

    JiraComponent(id, name, assigneeType){
        this.id = id
        this.name = name
        this.assigneeType = assigneeType
    }

    @Override
    String toString() {
        return "\"${name}\",\"${description}\",${lead},${assigneeType}"
    }

}