package jira

class JiraStatus implements Jiras {
    int id
    String name
    String description
    String color
    int usedInProjects = 0
    int usedInIssues = 0
    int usedInIssueTypes = 0

    JiraStatus(String id, name, description, color){
        this.id = id.toInteger()
        this.name = name
        this.description = description
        this.color = color
    }

    @Override
    String toString(){
        return "${id},${name},\"${description.replaceAll("\"", "\"\"")}\",${color},${usedInProjects},${usedInIssueTypes},${usedInIssues}"
    }

}
