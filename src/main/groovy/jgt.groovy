import base.AuthPanel
import base.ParametersStorage
import tools.ComponentSync
import tools.RapidViewChecker
import tools.StatusStatistic
import tools.UpdateFieldsByCsv

def jiraUrl = System.getProperty("jiraUrl")
def jiraUser = System.getProperty("jiraUser")
def jiraPass = System.getProperty("jiraPass")
def jiraCert = System.getProperty("jiraCert")
def jiraTool = System.getProperty("jiraTool")
def fromProj = System.getProperty("fromProj")
def toProj = System.getProperty("toProj")
def taskComp = System.getProperty("taskComp")
def csv = System.getProperty("csv")

AuthPanel panel = new AuthPanel()

if (jiraUrl == null || jiraUser == null || jiraPass == null) {
    panel.askLoginInfo()
} else {
    ParametersStorage.targetHost = jiraUrl
    ParametersStorage.setCredentials(jiraUser, jiraPass)

    if (jiraCert != null) {
        panel.disableSslForConnection()
    }
    if (panel.checkConnection()) {
        if (jiraTool == null) {
            //todo show panel with prompted login
        } else {
            switch (jiraTool){
                case "compSync" :
                    if (fromProj == null || toProj == null || taskComp == null) {
                        println "Wrong parameters."
                        help()
                    } else {
                        new ComponentSync().run(fromProj, toProj, taskComp)
                    }
                    break
                case "status":
                    new StatusStatistic().run()
                    break
                case "csvfield":
                    if(csv != null){
                        new UpdateFieldsByCsv().run(csv)
                    }else{
                        println "Wrong parameters."
                        help()
                    }
                    break
                default:
                    new RapidViewChecker().run()
            }
        }
    }
}

if (args.length > 0 && args.contains("help")) {
    this.help()
}

void help() {
    println """To use without UI necessary provide all arguments, except [optional]. By default run tool: check rapid views.
java -jar jgt.jar
-DjiraUrl=https://example.com
-DjiraUser=name 
-DjiraPass=password
[-DjiraCert=ignore] - java will trust any https server 
[-DjiraTool=
   [compSync -DfromProj=KEYA -DtoProj=KEYB -DtaskComp=compare], - run compnnents sync tool. Task should be: compare, copy, copyr, sync
   [status], - get statistic info for statuses
   [csvfield -Dcsv=PATH_TO_CSV_FILE] - update custom fields using CSV file
]
"""
}

